<?php
session_name("x?newsite");    
session_start();
include("../../libs/lib_config.php");
$inst = new Connexion();
include("../../libs/lib_visiteur.php");
include("libs/_lib_admin.php");
$visite = new visiteur ($p,$inst->bdd);
if(!isset($_SESSION['visiteur'])) { $visite->enregistre_visiteur(); }
else { $visite->actualise_visiteur();}
if(isset($_GET['p']))
{
	$p="404";
	if(file_exists("_pages/_page_".$_GET['p'].".php")) $p=(int)$_GET['p'];
}
else $p=1;
$admin= new admin($inst->bdd);
$admin->connexion();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>Administration</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/global.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="js/jquery.form.min.js"></script>
    <script type="text/javascript" src="js/images.js"></script>
</head>
<body>

	<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					ADMINISTRATION
				</a>
			</div>
			<div class="col w5 last right bottomlast">
				<p class="last">Niveau <span class="strong">Admin,</span> <a href=""><img src="images/ouvert.png" width="25px"/></a></p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
        	<div id="minwidth">
				<div id="holder">
					
            <?php
            if(file_exists("_includes/_menus.php")) require_once("_includes/_menus.php");
            ?>	
                    
            <div id="desc">
                <div class="body">
					<?php
					if(isset($_SESSION['valid_user']) && $_SESSION['valid_user']==hash('sha512',$admin->salt) ) require_once("_pages/_page_".$p.".php");
					else $admin->affiche_connexion(0);
                    //if(file_exists("_pages/_page_".(int)$p.".php")) require_once("_pages/_page_".$p.".php");
                    ?>		
                </div>
             </div>
        
        </div>
    
    
    <div id="footer">
        <p class="last">Copyright 2015 - Admin <a href="">Medya</a></p>
    </div>
</body>
</html>